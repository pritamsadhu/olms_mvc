<?php
$routes = [
    '/' => [
        'controller' => 'AuthController',
        'function'     => 'showBook'
    ],
    '/home' => [
        'controller' => 'User',
        'function'     => 'home'
    ],
    '/show' => [
        'controller' => 'TestController',
        'function'     => 'index'
    ],
    '/login' => [
        'controller' => 'User',
        'function'     => 'test_login'
    ],
    '/dologin' => [
        'controller' => 'AuthController',
        'function'     => 'postLogin'
    ],
    '/pritam' => [
        'controller' => 'User',
        'function'   => 'hello'
    ],
    '/welcome' => [
        'controller' => 'AuthController',
        'function' => 'showBook'
    ],
    '/borrow' => [
        'controller' => 'User',
        'function' => 'showBorrow'
    ],
    '/visitborrow' => [
        'controller' => 'AuthController',
        'function' => 'BorrowBook'
    ],
    '/doBorrow' => [
        'controller' => 'User' ,
        'function' => 'doBorrow'
    ],
    '/profile' =>[
        'controller' => 'User' ,
        'function' => 'profile'
    ],
    '/doReturn' =>[
        'controller' => 'User' ,
        'function' => 'doReturn'
    ],
    '/logout' => [
        'controller' => 'AuthController',
        'function' => 'doLogout'
    ],
    '/admin' => [
        'controller' => 'admin',
        'function' => 'adminhome'
    ],
    '/addUserpage' =>[
        'controller' => 'admin',
        'function' => 'addUserpage'
    ],
    '/addUser' =>[
        'controller' => 'admin',
        'function' => 'addUser'
    ],
    '/viewUserpage' =>[
        'controller' => 'admin',
        'function' => 'viewUser'
    ],
    '/deleteUser' =>[
        'controller' => 'admin',
        'function' => 'deleteUser'
    ],
    '/editUser' =>[
        'controller' => 'admin',
        'function' => 'editUser'
    ],
    '/lendingHistory' =>[
        'controller' => 'admin',
        'function' => 'lendingHistory'
    ],
    '/addbookpage' =>[
        'controller' => 'admin',
        'function' => 'addbookpage'
    ],
    '/addBook' =>[
        'controller' => 'admin',
        'function' => 'addBook'
    ],
    '/viewBookpage' =>[
        'controller' => 'admin',
        'function' => 'viewBookpage'
    ],
    '/bookSettings' =>[
        'controller' => 'admin',
        'function' => 'bookSettings'
    ],
    '/editBook' =>[
        'controller' => 'admin',
        'function' => 'editBook'
    ],

];
