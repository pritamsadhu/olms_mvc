<?php
/**
 * Created by PhpStorm.
 * User: monoit
 * Date: 3/2/17
 * Time: 3:25 PM
 */

namespace controller;
use core\Bootstrap;
use core\Session;
use library\Redirect;
use model\BookModel;
use library\Auth;

use controller\AuthController;
use model\BorrowModel;
use model\Model;

class User
{
    use Auth;

        public $test;
        function __construct()
        {
            $this->test = 'Hello';
        }

    //This method is used for show all users
    public function showUser() {

        Session::destroy();
        $bootstrap = new Bootstrap();

        $books = new BookModel();
        $bookData = $books->get();
        $bookData=$bookData->fetch_all();
        $bookData = array();



        $bootstrap->loadView('welcome',$bookData);
    }

    public function home()
    {
        echo Session::get('id');
        $bootstrap = new Bootstrap();
        $bootstrap->loadView('home');
    }

    //This method is used for create new users
    public function createUser(array $user_array) {

    }

    //This method is used for delete user
    public function deleteUser($user_id) {

    }

    //This method is used for show user by id
    public function showUserById($user_id) {

    }

    /**
     * @return string
     */
    public function test_login()
    {
        $bootstrap = new Bootstrap();
        $bootstrap->loadView('login');
    }

    //this method use to fetch all book details
    public function display_books(){
        $bootstrap=new Bootstrap();
    }

    public function showHome(){

        $bootstrap=new Bootstrap();
        $redirect_path='login';
        $auth_obj= new AuthController();
        $login=$auth_obj->checkLogin();
        if($login['login']==1)
        {
            $bootstrap->loadView('welcome');

        }
        else{
                $redirect_library = new Redirect();
                $redirect_library->go("login");
        }
    }

    public function showBorrow(){



        $bootstrap=new Bootstrap();
        var_dump($_POST['borrow_id']);
        if(isset($_POST['borrow_id']))
        {
            $bootstrap->loadView('borrow?id=5');

        }
        else
        {
            $bootstrap->loadView('borrow');

        }
    }

    public function doBorrow(){



        $login = new AuthController();
        if(isset($_SESSION['user_id']))
        {
            // from borrow page book id sent via a POST method and storing that bookid
            $bookid=$_POST['bookid'];
            //echo $bookid;
            // fetching user id from session
            $userid=Session::get('user_id');
            //updating the database status and borrow table
            //$sql="UPDATE books SET `status`='N' WHERE book_id='$bookid' ";
            $bookupdate=new BookModel();
            $update=array();
            $update['status']='N';
            $result=$bookupdate->update($update)->where(['book_id','=',$bookid])->get('anything');
            if($result)
            {

                $date=date("Y-m-d");
                $sql1="INSERT INTO borrow (`user_id`,`book_id`,`doi`) VALUES ('$userid','$bookid','$date')";
                $result1=$bookupdate->db_job($sql1)->get('anything');
                if($result1)
                {
                    $redirect=new Redirect();
                    $redirect->go('profile');
                }
                else
                {
                    echo "problem in Borrow insert ";
                }
            }
            else
            {
                echo "Something went wrong in update book status ";
            }
        }
        else
        {
            $redirect=new Redirect();
            $redirect->go('login');
        }






    }

    public function profile(){



        if(isset($_SESSION['user_id']))
        {

            $bootstrap=new Bootstrap();
            $userid=Session::get('user_id');
            $sql1="SELECT books.book_id,books.book_name,books.author_name,books.image,borrow.doi,borrow.borrow_id FROM books,borrow WHERE books.book_id=borrow.book_id AND borrow.user_id='$userid' AND borrow.dos=''";
            $sql="SELECT books.book_id,books.book_name,books.author_name,books.image,borrow.doi,borrow.borrow_id FROM books,borrow WHERE books.book_id=borrow.book_id AND borrow.user_id='$userid'";

            $bkmdl=new BookModel();
            $result=$bkmdl->db_job($sql)->get('anything');
            $result1=$bkmdl->db_job($sql1)->get('anything');
            $row=$bkmdl->fetch_data($result);
            $row1=$bkmdl->fetch_data($result1);
            $bootstrap->loadView('profile',array('book_data'=>$row,'book_data2'=>$row1));
        }
        else
        {
            $redirect=new Redirect();
            $redirect->go('login');
        }


    }

    public function doReturn(){



        $date=date("Y-m-d");
        $userid=Session::get('user_id');
        $bkmodel=new BookModel();
        $redirect=new Redirect();

        if(isset($_POST['check']))
        {
            foreach ($_POST['check'] as $key)
            {

//                $sql="UPDATE borrow SET `dos`='$date' WHERE `book_id`='$key' AND `user_id`='$userid'";
//                $sql1="UPDATE books SET `status`='Y' WHERE book_id='$key' ";
//
//                $result=$bkmodel->db_job($sql)->get('anything');
//                $result1=$bkmodel->db_job($sql1)->get('anything');

                //this update array for updating the borrow table
                $update=array();
                $update['dos']=$date;

                // this update1 table for updating the book status at book table
                $update1=array();
                $update1['status']='Y';
                $borrowmodel=new BorrowModel();
                $result=$borrowmodel->update($update)->where(['book_id','=',$key])->where(['user_id','=',$userid])->get('anything');
                $result1=$bkmodel->update($update1)->where(['book_id','=',$key])->get('anything');
                if($result AND $result1){
                    echo "Ok";
                }
                else
                {
                    die('Sorry something went wrong');
                }

            }
            $redirect->go('profile');

        }
        else{
            $redirect->go('profile');
        }

    }
}