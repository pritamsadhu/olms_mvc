<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 12/07/17
 * Time: 2:55 PM
 */

namespace controller;
use core\Bootstrap;
use core\Session;
use library\Redirect;
use model\BookModel;
use library\Auth;

use controller\AuthController;
use model\Model;
use model\UserModel;

class admin
{
    use Auth;
    public $test;

    function __construct()
    {
        $this->test = 'Hello';
    }

    public function adminhome(){

        $security=new AuthController();
        $security->admincheck();



                $bootstrap=new Bootstrap();
                $bootstrap->loadView('admin/admin');



    }

    public function addUser(){
        $security=new AuthController();
        $security->admincheck();
        if(isset($_POST['fname']))
        {
            $fname=$_POST['fname'];
            $lname=$_POST['lname'];
            $email=$_POST['email'];
            $password=$_POST['password'];
            $type=$_POST['type'];

            $sql="INSERT INTO user (`user_fname`,`user_lname`,`user_email`,`user_password`,`user_type`) VALUES ('$fname','$lname','$email','$password','$type')";
            $usr=new UserModel();
            $result=$usr->db_job($sql)->get('anything');
            if($result)
            {
                $redirect=new Redirect();
                $redirect->go('viewUserpage');
            }
            else
            {
                echo "something went wrong";
            }
        }
        else
        {
            $redirect=new Redirect();
            $redirect->go('admin');
        }

    }

    public function addUserpage()
    {
        $security=new AuthController();
        $security->admincheck();
        $bootstrap =new Bootstrap();
        $bootstrap->loadView('admin/addUserpage');
    }

    public function viewUser(){
        $security=new AuthController();
        $security->admincheck();


        $usermdl=new UserModel();
        $usrdata=$usermdl->get('whole');
        $usrdata=$usrdata->fetch_all();
//        var_dump($usrdata);
        $bootstrap =new Bootstrap();
        $bootstrap->loadView('admin/viewUser',array('user'=>$usrdata));
    }

    public function deleteUser(){
        $security=new AuthController();
        $security->admincheck();


        if($_POST['action']=='Delete')
        {
            $data=$_POST['check'];
            $usermodel=new UserModel();
            foreach ($data as $key)
            {
                $sql="DELETE FROM user WHERE `user_id`='$key'";
                $result=$usermodel->db_job($sql)->get('anything');
                if($result)
                {

                }
                else{
                    echo "Delete failed , something went wrong";
                    die();
                }
            }
            $redirect=new Redirect();
            $redirect->go('viewUserpage');
        }
        else
        {
            $userid=$_POST['action'];
            $usr=new UserModel();
            $user  = $usr->where(['user_id','=',$userid])->get('login');
            $user=$user->fetch_all();
            $loadview=new Bootstrap();
            $loadview->loadView('admin/editUser',array('user_data'=>$user));

        }
//
//        if($_POST['action']=='Edit')
//        {
//            var_dump($_POST['id']);
//        }
    }

    public function editUser(){
        $security=new AuthController();
        $security->admincheck();


        $user_data=$_POST;
        $sql="UPDATE user SET `user_fname`='$user_data[user_fname]',`user_lname`='$user_data[user_lname]',`user_email`='$user_data[user_email]',`user_type`='$user_data[user_type]' WHERE `user_id`='$user_data[user_id]'";
        $usmdl=new UserModel();

        //data to be updated
        $update=array();
        $update['user_fname']=$user_data['user_fname'];
        $update['user_lname']=$user_data['user_lname'];
        $update['user_email']=$user_data['user_email'];
        $update['user_type']=$user_data['user_type'];

        $result=$usmdl->where(['user_id','=',$user_data['user_id']])->update($update);
        //$result=$usmdl->update($update)->where(['user_id','=',$user_data['user_id']])->get('anything');
        if($result)
        {
            echo "Update Successfull";
        }
        else
        {
            echo "Something went wrong try again";
        }
    }

    public function lendingHistory(){
        $security=new AuthController();
        $security->admincheck();

        $usrmdl=new UserModel();
        $sql="SELECT books.book_id,books.book_name,books.status,user.user_fname,user.user_lname,borrow.borrow_id,borrow.doi,borrow.dos FROM books,user,borrow WHERE books.book_id=borrow.book_id AND user.user_id=borrow.user_id";
        $result=$usrmdl->db_job($sql)->get('anything');
        $result=$result->fetch_all();
       // $lending_data=$usrmdl->fetch_data($result);
        $btstrp=new Bootstrap();
        $btstrp->loadView('admin/lendingHistory',array('lending_data'=>$result));

    }

    public function addbookpage(){
        $security=new AuthController();
        $security->admincheck();


        $btstrp=new Bootstrap();
        $btstrp->loadView('admin/addbookpage');
    }

    public function addBook(){
        $security=new AuthController();
        $security->admincheck();


        if(isset($_POST['book_name']))
        {
            $bookname=$_POST['book_name'];
            $authorname=$_POST['author_name'];
            $imagename=$_FILES['image']['name'];
            $imagetmpname=$_FILES['image']['tmp_name'];
            $category=$_POST['category'];
            $description=$_POST['description'];
            $btstrp=new Bootstrap();

            //$location=$btstrp::pathAsset('uploaded_images/').$imagename;
            $location=$_SERVER['DOCUMENT_ROOT'].'/custom_framework/assets/uploaded_images/'.$imagename;
          //  move_uploaded_file($imagetmpname,$location);
            if(move_uploaded_file($imagetmpname,$location))
            {
                $sql="INSERT INTO books (`book_name`,`author_name`,`image`,`category`,`description`,`status`) VALUES ('$bookname','$authorname','$imagename','$category','$description','Y')";
                $usrmdl=new UserModel();
                $result=$usrmdl->db_job($sql)->get('anything');
                if($result)
                {
                    $redirect=new Redirect();
                    $redirect->go('viewBookpage');
                }
                else
                {
                    echo "Something went wrong";
                }

            }


        }
    }

    public function viewBookpage(){
        $security=new AuthController();
        $security->admincheck();



        $bookmodel=new BookModel();
        $bookdata=$bookmodel->get('whole');
        $bookdata=$bookdata->fetch_all();
        //var_dump($bookdata);
        $btstrp=new Bootstrap();
        $btstrp->loadView('admin/viewBookpage',array('bookdata'=>$bookdata));
    }

    public function bookSettings(){
        $security=new AuthController();
        $security->admincheck();


        if($_POST['action']=='Delete')
        {
            $data=$_POST['check'];
            $bookmodel=new BookModel();
            foreach ($data as $key)
            {
                $sql="DELETE FROM books WHERE `book_id`='$key'";
                $result=$bookmodel->db_job($sql)->get('anything');
                if($result)
                {

                }
                else{
                    echo "Delete failed , something went wrong";
                    die();
                }
            }
            $redirect=new Redirect();
            $redirect->go('viewBookpage');
        }
        else
        {
            $bookid=$_POST['action'];
            $book=new BookModel();
            $book= $book->where(['book_id','=',$bookid])->get('login');
            $book=$book->fetch_all();
            $loadview=new Bootstrap();
            $loadview->loadView('admin/editBook',array('book_data'=>$book));

        }
    }

    public function editBook(){
        $security=new AuthController();
        $security->admincheck();


        $bkmdl=new BookModel();
        $book_data=$_POST;

        if($_FILES['image']['name']!='')
        {
            $imagename=$_FILES['image']['name'];
            $imagetempname=$_FILES['image']['tmp_name'];
            $location=$_SERVER['DOCUMENT_ROOT'].'/custom_framework/assets/uploaded_images/'.$imagename;

            if(move_uploaded_file($imagetempname,$location))
            {
                $bkmdl=new BookModel();
                $update_data = array();
                $update_data['book_name'] =$book_data['book_name'];
                $update_data['author_name']=$book_data['author_name'];
                $update_data['image']=$imagename;
                $update_data['category']==$book_data['category'];
                $update_data['description']=$book_data['description'];

                $result=$bkmdl->where(['book_id','=',$book_data['book_id']])->update($update_data);
//                $sql="UPDATE books SET book_name='$book_data[book_name]',author_name='$book_data[author_name]',image='$imagename',category='$book_data[category]',description='$book_data[description]' WHERE book_id='$book_data[book_id]'";
                if($result)
                {
                    $redirect=new Redirect();
                    $redirect->go('viewBookpage');
                }
                else
                {
                    echo "Upload failed on new image upload";
                }

            }
            else{
                echo "Something went wrong on uploading";
            }

        }
        else
        {
            $imagename=$_POST['image_preset'];

            //$sql="UPDATE books SET book_name='$book_data[book_name]',author_name='$book_data[author_name]',image='$imagename',category='$book_data[category]',description='$book_data[description]' WHERE book_id='$book_data[book_id]'";

            //data to be updated
            $update=array();
            $update['book_name']=$book_data['book_name'];
            $update['author_name']=$book_data['author_name'];
            $update['image']=$imagename;
            $update['category']=$book_data['category'];
            $update['description']=$book_data['description'];

            $res=$bkmdl->where(['book_id','=',$book_data['book_id']])->update($update);
            if($res)
            {
                $redirect=new Redirect();
                $redirect->go('viewBookpage');
            }
            else
            {
                echo "Upload failed on old image upload";
            }

        }

    }
}