<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 14/07/17
 * Time: 6:29 PM
 */

namespace model;
use model\Model;

class BorrowModel extends Model
{
    protected $table ='borrow';
    function __construct()
    {
        parent::__construct($this->table);
    }
}