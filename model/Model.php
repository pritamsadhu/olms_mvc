<?php
/**
 * Created by PhpStorm.
 * User: monoit
 * Date: 7/2/17
 * Time: 7:25 PM
 */

namespace model;
use core\Database;

class Model
{
    protected $query;
    private $table;
    function __construct($table)
    {
        $this->table = $table;
        $this->query="";
    }

    //This method is used for get data from a table
    public function get($action)
    {
        /* If you calling get() then someparameter need to pass
            login-> if you have select query with some condition
            whole-> if you want to fetch all data without any condition
            anything-> if you have your own sql query
         *
         */
        //$this->query= 'SELECT * FROM '.$this->table.' WHERE 1';
        $mysqli = new Database();
        $sql = "SELECT * FROM ".$this->table." WHERE 1";
        if($action=='login')
        {

            $this->query=$sql." ".$this->query;
        }
        else if($action=='whole')
        {
           $this->query=$sql;
        }
        else
        {

        }


        $result = $mysqli->connection->query($this->query);
        if(!empty($result)) {
////            $row = $result->fetch_array();
//              $row=$result->fetch_all();
//            return $row;
            return $result;
        }
        return array();

    }



    //This method is used for implement where condition in sql query
    public function where(array $where_data) {


        if(is_string($where_data[2])) {
            $where_data[2] = "'".$where_data[2]."'";
        }

             $this->query .=" AND ".$where_data[0]." ".$where_data[1]." ".$where_data[2];
             return $this;




    }


    public function update(array $update_data){
        $update_query="UPDATE ".$this->table." SET";
        $mysqli = new Database();
       // $this->query="UPDATE ".$this->table." SET";
        $number=count($update_data);
        $i=1;
            foreach ($update_data as $key=>$value)
            {

                if(is_string($key)) {
                    $value = "'".$value."'";
                }

                // this condition is using to check whether the condition is last or not
                if($i==$number){
                    $update_query .=" ".$key. "=" .$value. " WHERE 1 ";
                }
                else
                {
                    $update_query .=" ".$key. "=" .$value. ",";
                }

                $i++;
            }
//

//        $this->query .=$update_data[0]." ".$update_data[1]." ".$update_data[2];
        $this->query=$update_query." ".$this->query;


            $result = $mysqli->connection->query($this->query);
        if(!empty($result)) {
////            $row = $result->fetch_array();
//              $row=$result->fetch_all();
//            return $row;
            return $result;
        }
        return array();
        return $this;


    }

    public function db_job($sql){
        //$mysqli =new Database();
        $this->query=$sql;
        //$result = $mysqli->connection->query($sql);
        //mysqli_error();
        return $this;

    }

    public function fetch_data($result)
    {
        $return_data=mysqli_fetch_all($result);
        return $return_data;
    }

}