<?php
    $config = [
        'default' => 'mysql',
        'connections' => [
            'mysql' => [
                'driver' => 'mysql',
                'host'   => 'localhost',
                'database' => 'olms',
                'username' => 'root',
                'password' => 'root'
            ],
        ],
    ];

//$BASE_PATH = 'localhost/custom_framework/assets';


?>