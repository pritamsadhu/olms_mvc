<html>

    <head>
        <title>Online Library Management System</title>
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <?php
            include "header.php";
        ?>

    </head>

    <body>

    <?php //var_dump($bookData); ?>

        <div class="container">
            <div class="ui-widget">
                <input type="text" name="books" class="search" placeholder="Search Box" id="tags">
            </div>

            <?php
                foreach ($book_data as $key=>$value)
                {
                    ?>
                    <div class="item">
                        <?php
                            $location="uploaded_images/".$value[3];

                        ?>
                        <img class="picture" src="<?php echo $bootstrap::pathAsset('uploaded_images/'.$value[3]); ?>">
                        <div class="title">
                            <p><?php echo $value[1]; ?><p>
                        </div>
                        <div class="des">
                            <p><?php echo $value[2]; ?></p>
                        </div>
                        <div class="borrowbtn">
                            <form method="post" action="<?php echo $bootstrap::pathTo('visitborrow') ?>">
                            <input type="hidden" name="bookid" value="<?php echo $value[0]; ?>">
                            <input type="submit" class="btn btn-info" value="Borrow">
                            </form>

                        </div>

                    </div>

            <?php

                }
            ?>

        </div>

    </body>
</html>