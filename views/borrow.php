<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 10/07/17
 * Time: 5:36 PM
 */
    include "header.php";
?>
<html>
    <head>
        <title>Borrow Page</title>
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?> ">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="com-md-4">
                    <div class="item" style="border: 1px solid black">
                        <img src="<?php echo $bootstrap::pathAsset('uploaded_images/'.$book_data[0][3]) ?>" class="picture1" >
                    </div>
                </div>
                <div class="col-md-8">
                    <form method="post" action="doBorrow">

                        <div class="bheader">
                            <?php echo $book_data[0][1]; ?>

                        </div>
                        <div style="clear: both">
                            Raing:
                            <img src="<?php echo $bootstrap::pathAsset('images/rating.png'); ?>" width="60px" height="10px">
                        </div>
                        <div style="clear: both">
                            <img src="<?php echo $bootstrap::pathAsset('images/author.png'); ?>" width="30px" height="30px">
                            <span> By:&nbsp;<?php echo $book_data[0][2];  ?></span>
                        </div>
                        <div style="clear: both;margin-top: 2px;">
                            <img src="<?php echo $bootstrap::pathAsset('images/lend.png'); ?>" width="30px" height="30px">
                            <span>Borrowed By : 100 Users</span>
                        </div>
                        <div id="bdescription">
                            <img src="<?php echo $bootstrap::pathAsset('images/description.png'); ?>" width="30px" height="30px">
                            <?php echo $book_data[0][5];?>
                        </div>
                        <input type="hidden" name="bookid" value="<?php echo $book_data[0][0]; ?>">
                        <?php
                            if($book_data[0][6]=='Y')
                            {
                                ?>
                                <input type="submit" class="btn btn-info" value="Borrow" style="margin-top: 10px">

                                <?php
                            }
                            else
                            {
                                ?>
                                <input type="submit" class="btn btn-info" value="Borrow" disabled style="margin-top: 10px">
                                <img src="<?php echo $bootstrap::pathAsset('images/out-of-stock.png'); ?> " style="width: 100px; height: 100px;">
                            <?php
                            }
                        ?>
                    </form>
                </div>
            </div>


        </div>
    </body>
</html>
