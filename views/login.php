<html>
<head>
    <title>Login Page</title>
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<?php include "header.php" ?>
<div class="container mag">
    <form action="<?php echo $bootstrap::pathTo('dologin');?>" method="post" name="loginform">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2" style="text-align: center">Login System</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" class="box"> </td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" class="box"></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="btn" value="Login"></td>
            </tr>
            </tbody>
        </table>
    </form>

</body>
</html>