<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 13/07/17
 * Time: 6:17 PM
 */

?>
<html>
<head>
    <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?>">
    <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('css/bootstrap.min.css');  ?>">
    <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('css/bootstrap-theme.min.css'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">

    <script src="<?php echo $bootstrap::pathAsset('Design/js/bootstrap.min.js'); ?>"></script>
</head>
<body>
<div class="nav">
    <a href="<?php echo $bootstrap::pathTo('admin'); ?>"><img src="<?php echo $bootstrap::pathAsset('images/library.png'); ?>" width="64px" height="64px"> </a>
    <span class="nav_logo">Online Library Management System ADMIN</span>
    <?php
    if(isset($_SESSION['user_id']))
    {

        ?>
        <a href="<?php echo $bootstrap::pathTo('logout'); ?>"><img src="<?php echo $bootstrap::pathAsset('images/logout.png') ?>" style="height: 50px;width: 50px;float: right;padding: 5px"> </a>

        <?php
    }
    else
    {
        ?>
        <a href="<?php echo $bootstrap::pathTo('login'); ?>"><img src="<?php echo $bootstrap::pathAsset('images/login.png') ?>" style="height: 50px;width: 50px;float: right;padding: 5px"> </a>
        <?php
    }

    ?>
</div>
</body>
</html>
