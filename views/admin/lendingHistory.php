<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 13/07/17
 * Time: 2:45 PM
 */
//var_dump($lending_data);
include "header_admin.php";

?>
<html>
    <head>
        <title>Lending History</title>
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?> ">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
    <div class="container" id="all">
        <table class="table table-bordered">
            <thead>
            <th>Transaction No</th>
            <th>Book Name</th>
            <th>Borrowed By</th>
            <th>Date of issue</th>
            <th>Date of Submission</th>
            </thead>
            <tbody>
            <?php
            //var_dump($book_data);
            foreach ($lending_data as $key=>$value)
            {
                ?>
                <tr>
                    <td><?php echo $value[5];  ?></td>
                    <td><?php echo $value[1]; ?></td>
                    <td><?php echo $value[3].' '.$value[4]; ?></td>
                    <td><?php echo $value[6]; ?></td>
                    <td><?php echo $value[7]; ?></td>

                </tr>


                <?php
            }

            ?>
            </tbody>
        </table>
    </div>
    </body>
</html>

