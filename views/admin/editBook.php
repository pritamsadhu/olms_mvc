<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 13/07/17
 * Time: 12:18 PM
 */
include "header_admin.php";


?>
<html>
<head>
    <title>Edit Books</title>
    <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">
    <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?> ">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<div class="container">
    <form method="POST" action="<?php echo $bootstrap::pathTo('editBook'); ?>" enctype="multipart/form-data">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td colspan="2">Book Edit Zone</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Book Id</td>
                <td> <input type="text" disabled name="" value="<?php echo $book_data[0][0]; ?>"> </td>
                <input type="hidden"  name="book_id" value="<?php echo $book_data[0][0];; ?>">
            </tr>
            <tr>
                <td>Book Name</td>
                <td> <input type="text" name="book_name" value="<?php echo $book_data[0][1]; ?>"> </td>

            </tr>
            <tr>
                <td>Author Name</td>
                <td><input type="text" name="author_name" value="<?php echo $book_data[0][2]; ?>"> </td>
            </tr>
            <tr>
                <td>Category</td>
                <td><input type="text" name="category" value="<?php echo $book_data[0][4]; ?>"></td>
            </tr>
            <tr>
                <td>Description</td>

                <td><input type="text" name="description" value="<?php echo $book_data[0][5]; ?>"></td>


            </tr>
            <tr>
                <td>Book Cover</td>
                <td><input type="file" name="image" ></td>
                <input type="hidden" name="image_preset" value="<?php echo $book_data[0][3]; ?>">
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Edit User" class="btn btn-success"></td>

            </tr>
            </tbody>
        </table>
    </form>
</div>
</body>
</html>
