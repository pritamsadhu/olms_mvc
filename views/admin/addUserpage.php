<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 12/07/17
 * Time: 4:34 PM
 */
include "header_admin.php";

?>
<html>
    <head>
        <title>Add User OLMS</title>
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?> ">


        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
    <div class="container">
        <form method="post" action="<?php echo $bootstrap::pathTo('addUser'); ?>">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="2" style="border-bottom: 1px solid black"> User Registration</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>First Name</td>
                    <td><input type="text" name="fname"> </td>
                </tr>

                <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="lname"> </td>

                </tr>

                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email"> </td>

                </tr>

                <tr>
                    <td>Password</td>
                    <td><input type="text" name="password"> </td>

                </tr>

                <tr>
                    <td>User Type</td>
                    <td>
                        <select name="type">
                            <option value="admin"> Admin </option>
                            <option value="normal"> User</option>
                        </select>
                    </td>

                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Register" class="btn btn-info"> </td>
                </tr>


                </tbody>
            </table>
        </form>
        <a href="admin.php"><img src="<?php echo $bootstrap::pathAsset('images/back.png'); ?>" width="40px" height="40px"></a>

    </div>
    </body>
</html>
