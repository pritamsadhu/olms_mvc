<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 12/07/17
 * Time: 3:16 PM
 */
include "header_admin.php";
?>
<html>
    <head>
        <title>OLMS Admin Panel</title>
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?> ">


        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
        <div class="container">
            <table class="table table-bordered" style="margin: 5px">
                <thead>
                    <tr style="background-color: #5cb85c" >
                        <td colspan="2">Admin Panel of OLMS</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="<?php echo $bootstrap::pathTo('addUserpage'); ?>" class="btn btn-primary">Add User</a></td>
                        <td><a href="<?php echo $bootstrap::pathTo('addbookpage'); ?>" class="btn btn-warning">Add Books</a></td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo $bootstrap::pathTo('viewUserpage'); ?>" class="btn btn-danger">User Settings</a> </td>
                        <td><a href="<?php echo $bootstrap::pathTo('viewBookpage'); ?>" class="btn btn-info">Book Settings</a> </td>
                    </tr>
                    <tr>
                        <td><a href="<?php echo $bootstrap::pathTo('lendingHistory'); ?>" class="btn btn-success">Lending History</a> </td>
                        <td><a href="#" class="btn btn-danger" disabled="">NULL</a> </td>
                    </tr>
                </tbody>

            </table>
        </div>
    </body>
</html>
