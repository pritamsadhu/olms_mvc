<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 13/07/17
 * Time: 4:51 PM
 */
//var_dump($bookdata);
include "header_admin.php";

?>
<html>
    <head>
        <title>OLMS Book Management System</title>
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?> ">


        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
    <div class="container">
        <form action="<?php echo $bootstrap::pathTo('bookSettings'); ?>" method="post">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th colspan="8">Registered Books</th>

                </tr>
                <tr>
                    <th>Book Id</th>
                    <th>Book Name</th>
                    <th>Author Name</th>
                    <th>Image</th>
                    <th>Category</th>
                    <th>Description</th>

                    <th>Mark</th>
                    <th>Edit Book</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($bookdata as $key=>$value)
                {
                    ?>
                    <tr>
                        <td><?php echo $value[0]; ?></td>
                        <td><?php echo $value[1]; ?></td>
                        <td><?php echo $value[2]; ?></td>
                        <td><?php echo $value[3]; ?></td>
                        <td><?php echo $value[4]; ?></td>
                        <td><?php echo $value[5]; ?></td>
                        <td><input type="checkbox" name="check[]" value="<?php echo $value[0]; ?>"></td>
                        <!--                            <input type="hidden" name="id" value="--><?php // echo $value[0];?><!--">-->
                        <td>
                            <button type="submit" name="action" class="btn btn-info" value="<?php echo $value[0]; ?>">Edit</button>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <input type="submit" class="btn btn-info" name="action" value="Delete">
        </form>
    </div>
    </body>
</html>
