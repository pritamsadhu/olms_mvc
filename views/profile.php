<?php
/**
 * Created by PhpStorm.
 * User: pritamsadhu
 * Date: 11/07/17
 * Time: 2:51 PM
 */
    include "header.php";
?>
<html>
    <head>
        <title>Book Lending History</title>
        <link href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $bootstrap::pathAsset('jquery-3.2.1.min.js');  ?>"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('resources/demos/style.css');?> ">
        <link rel="stylesheet" href="<?php echo $bootstrap::pathAsset('mystyle.css');?> ">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
    <div class="container-fluid">
        <select name="data_type" style="margin-top: 10px">
            <option value="not_returned">Display only returned books</option>
            <option value="all">Display all lending history</option>

        </select>
    </div>
    <div class="container-fluid" style="margin-top: 20px;border: 1px solid black">
        <div class="container" id="all">
            <table class="table">
                <thead>
                    <th>Transaction No</th>
                    <th>Book Name</th>
                    <th>Author Name</th>
                    <th>Date of issue</th>
                    <th>Check</th>
                </thead>
                <tbody>
            <?php
                //var_dump($book_data);
                foreach ($book_data as $key=>$value)
                {
                    ?>
                    <tr>
                        <td><?php echo $value[5];  ?></td>
                        <td><?php echo $value[1]; ?></td>
                        <td><?php echo $value[2]; ?></td>
                        <td><?php echo $value[4]; ?></td>

                    </tr>


                <?php
                }

            ?>
                </tbody>
            </table>
        </div>
        <div class="container" id="specific">
           <form method="post" action="<?php echo $bootstrap::pathTo('doReturn'); ?>">
            <table class="table">
                <thead>
                <th>Transaction No</th>
                <th>Book Name</th>
                <th>Author Name</th>
                <th>Date of issue</th>
                <th>Check</th>
                </thead>
                <tbody>
                <?php
                foreach ($book_data2 as $key=>$value)
                {
                    ?>
                    <tr>
                        <td><?php echo $value[5];  ?></td>
                        <td><?php echo $value[1]; ?></td>
                        <td><?php echo $value[2]; ?></td>
                        <td><?php echo $value[4]; ?></td>
                        <td> <input type="checkbox" name="check[]" value="<?php echo $value[0]; ?>"></td>

                    </tr>


                    <?php
                }
                ?>

                <tr>
                    <td><input type="submit" value="Return" class="btn btn-info"></td>

                </tr>

                </tbody>
            </table>
           </form>
        </div>
    </div>
        <script>
            $(document).ready(function () {
                $('#all').hide();
                $('#specific').show();
            })
            $('select').on('change', function() {
                if(this.value=='all')
                {
                   $('#all').show();
                   $('#specific').hide();
                }
                else if(this.value=='not_returned')
                {
                    $('#all').hide();
                    $('#specific').show();
                }
            })
        </script>
    </body>
</html>
