<?php
/**
 * Created by PhpStorm.
 * User: monoit
 * Date: 9/2/17
 * Time: 5:11 PM
 */

namespace library;
use controller\AuthController;
use core\Session;
use model\BookModel;
use model\UserModel;
use core\Bootstrap;
use library\Redirect;

trait Auth
{
    protected $redirect_path = 'welcome';
    protected $redirect;

    //This method is used for show login form
    public function showLogin()
    {
        $this->redirectIfLogedin();
        $bootstrap = new Bootstrap();
        $bootstrap->loadView('login');
    }

    //This method is used for handel post login
    public function postLogin()
    {
        $data = $_POST;
        $this->doLogin($data);

    }

    //This method is used for do login
    public function doLogin(array $auth_data)
    {
        $user_model = new UserModel();
        $user_data  = $user_model->where(['user_email','=',$auth_data['email']])->where(['user_password','=',(string)$auth_data['password']])->get('login');
        $user_data=$user_data->fetch_all();
        if(!empty($user_data)) {
            Session::set(['user_id' => $user_data[0][0],'email' => $user_data[0][3],'type' => $user_data[0][5]]);
            //$user_data;
            $redirect_library = new Redirect();
            if($user_data[0][5]=='admin')
            {
                $redirect_library->go('admin');

            }
            else
            {
                $redirect_library->go($this->redirect_path);

            }
        }
        else {

        }
    }

    //This method is used for check login
    public function checkLogin()
    {
        if(isset($_SESSION['user_id']))
        {
            $session_user_id = Session::get('user_id');

        }
        else
        {
            $session_user_id="";
        }
        if(!empty($session_user_id)) {
            return array('login' => 1);
        }
        else {
            return array('login' => 0);
        }
    }

    //this method check admin logged in or not
    public function checkadminLogin()
    {
        if(isset($_SESSION['type']))
        {
            $session_user_type = Session::get('type');

        }
        else
        {
            $session_user_type="";
        }
        if($session_user_type=='admin') {
            return array('login' => 1);
        }
        else {
            return array('login' => 0);
        }
    }
    
    //This method is redirect is login
    public function redirectIfLogedin()
    {
        $session_user_id = Session::get('user_id');
        if(!empty($session_user_id)) {
            $redirect_library = new Redirect();
            $redirect_library->go($this->redirect_path);
        }

    }

    public function showBook()
    {
        $bookmodel=new BookModel();
        $bookdata=$bookmodel->get('whole');
        $bookdata=$bookdata->fetch_all();
        //var_dump($bookdata);
        $bootstrap = new Bootstrap();
        $bootstrap->loadView('welcome',array('book_data' => $bookdata));



    }

    public function BorrowBook(){

        if(isset($_SESSION['user_id']))
        {

            if(isset($_POST['bookid']))
            {
                $bookid=$_POST['bookid'];
                $bookmodel=new BookModel();
                $bookdata=$bookmodel->where(['book_id','=',$bookid])->get('login');
                $bookdata=$bookdata->fetch_all();
                //var_dump($bookdata);
                $bootstrap= new Bootstrap();
                $bootstrap->loadView('borrow',array('book_data'=>$bookdata));
            }
            else
            {
                $redirect=new Redirect();
                $redirect->go('welcome');
            }
        }
        else
        {
            $redirect=new Redirect();
            $redirect->go('login');
        }

    }

    public function doLogout(){
        $session=new Session();
        $session->delete('user_id');
        $session->delete('user_email');

        $redirect=new Redirect();
        $redirect->go('welcome');
    }

    public function admincheck()
    {
        if(isset($_SESSION['user_id']))
        {
            if($_SESSION['type']!='admin')
            {
                $redirect=new Redirect();
                $redirect->go('logout');
            }
        }
        else
        {
            $redirect=new Redirect();
            $redirect->go('login');
        }
    }

    public function usercheck()
    {
        if(isset($_SESSION['user_id']))
        {
            if($_SESSION['type']!='user')
            {
                $redirect=new Redirect();
                $redirect->go('logout');
            }
        }
        else
        {
            $redirect=new Redirect();
            $redirect->go('login');
        }
    }
}